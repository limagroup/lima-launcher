#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/select.h>

#define STDIN_FD	0

void read_stdin(char *buffer, int len, int *nchars)
{
	int ret, left = len - *nchars - 1;
	char *p;

	if (left == 0) {
		printf("No space left in buffer!\n");
		return;
	}

	p = buffer + *nchars;
	ret = read(STDIN_FD, p, left);
	if (ret < 0)
		exit(4);
	else if (ret == 0)
		exit(0);
	*nchars += ret;
	buffer[*nchars] = '\0';
	while (*nchars > 0) {
		p = strchr(buffer, '\n');
		if (p == NULL)
			break;
		*p++ = '\0';
		if (strcasecmp(buffer, "quit") == 0)
			exit(0);
		left = buffer + *nchars - p;
		memmove(buffer, p, left + 1);
		*nchars = left;
	}
}

int main(int argc, char *argv[])
{
	const char *p, *fname = "/proc/sys/net/core/rmem_max";
	char buffer[1024];
	int len, ret, fd, flags, nfds, nchars;
	fd_set read_fds;
	struct timeval timeout;

	if (argc != 2)
		exit(1);

	flags = fcntl(STDIN_FD, F_GETFL, 0);
	if (flags < 0)
		exit(2);

	flags |= O_NONBLOCK;
	ret = fcntl(STDIN_FD, F_SETFL, flags);
	if (ret < 0)
		exit(3);

	nchars = 0;

	while (1) {
		fd = open(fname, O_WRONLY);
		if (fd < 0)
			exit(5);

		for (p = argv[1]; *p; p += ret)
			if ((ret = write(fd, p, strlen(p))) < 0)
				exit(6);

		if (close(fd) < 0)
			exit(7);

		FD_ZERO(&read_fds);
		FD_SET(STDIN_FD, &read_fds);
		nfds = STDIN_FD + 1;

		timeout.tv_sec = 10;
		timeout.tv_usec = 0;

		ret = select(nfds, &read_fds, NULL, NULL, &timeout);
		if (ret < 0)
			exit(3);
		else if (ret == 1)
			read_stdin(buffer, sizeof(buffer), &nchars);

	}

	return 0;
}
