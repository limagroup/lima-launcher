#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/resource.h>

int main(int argc, char *argv[])
{
    struct rlimit rlim = {RLIM_INFINITY, RLIM_INFINITY};
    int res;
    res = setrlimit(RLIMIT_MEMLOCK, &rlim);
    if (res < 0) {
	fprintf(stderr, "Error seting RLIMIT_MEMLOCK: %s\n", strerror(errno));
	exit(1);
    }

    if (argc < 2) {
	fprintf(stderr, "Invalid command argument length\n");
	exit(2);
    }

    execvp(argv[1], &argv[1]);
    fprintf(stderr, "Executing command: %s\n", strerror(errno));
    exit(1);
}
