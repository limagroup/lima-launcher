#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char *argv[])
{
	char *cpu, *state, *disable, *p, fname[256];
	int len, fd, ret;

	if (argc != 4)
		exit(1);

	cpu = argv[1];
	state = argv[2];
	disable = argv[3];

	if (!strlen(cpu) || !strlen(state) || !strlen(disable))
		exit(2);
	if (strcmp(state, "0") == 0)
		exit(3);
	len = sizeof(fname);
	ret = snprintf(fname, len,
		       "/sys/devices/system/cpu/cpu%s/cpuidle/state%s/disable",
		       cpu, state);
	if ((ret < 0) || (ret >= len))
		exit(4);

	fd = open(fname, O_WRONLY);
	if (fd < 0)
		exit(5);

	for (p = disable; *p; p += ret)
		if ((ret = write(fd, p, strlen(p))) < 0)
			exit(6);

	if (close(fd) < 0)
		exit(7);

	return 0;
}
