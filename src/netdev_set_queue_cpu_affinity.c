#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char *argv[])
{
	char *dev, *irq_queue, *p, fname[256];
	int irq, xps, rps, fd, len, ret;

	if (argc != 5)
		exit(1);
	irq = (strcmp(argv[1], "-i") == 0);
	xps = (strcmp(argv[1], "-t") == 0);
	rps = (strcmp(argv[1], "-r") == 0);
	if (!irq && !xps && !rps)
		exit(2);
	if (!strlen(argv[2]) || !strlen(argv[3]) || !strlen(argv[4]))
		exit(2);

	dev = argv[2];
	irq_queue = argv[3];

	len = sizeof(fname);
	if (irq)
		ret = snprintf(fname, len, "/proc/irq/%s/smp_affinity",
			       irq_queue);
	else
		ret = snprintf(fname, len, "/sys/class/net/%s/queues/%s/%cps_cpus",
			       dev, irq_queue, xps ? 'x' : 'r');
	if ((ret < 0) || (ret >= len))
		exit(3);

	fd = open(fname, O_WRONLY);
	if (fd < 0)
		exit(4);

	for (p = argv[4]; *p; p += ret)
		if ((ret = write(fd, p, strlen(p))) < 0)
			exit(5);

	if (close(fd) < 0)
		exit(6);
	return 0;
}
