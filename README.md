# LImA Launcher

This project collects the tools for launching **LImA** & **LImA2** applications. The main script `lima_launcher` aims to setup the detector backend computer(s) into the appropriate environment for executing **high-performance DAQ & processing applications**. Computer resources like CPU & memory are allocated on execution time before starting the application and released to the Operating System (OS) once the application finishes.

One key feature in `lima_launcher` is the configuration CPU scheduling & NUMA affinity, in order to optimize run-time performance. The `set_cpu_affinity` helper reads a JSON configuration file (created by `lima_launcher`) and modifies the CPU affinities of the network devices (netdev) IRQs & packet dispatching tasklets, as well as all the other processes running in the OS. All the CPUs' frequency governors are also set to *performance*.

`lima_launcher` can also set the `LD_PRELOAD` environment variable, activating specific implementations of standard subsystems, like the **Power9 NX GZIP hardware acceleration** through the third-party `power-gzip nxz` library.

Future implementations will include the configuration of temporary RAM filesystems (*tmpfs*) in order to optimize huge memory allocations by **LImA** applications.

`lima_launcher` & co. are currently designed to work with `supervisor`, although other frameworks like `systemd` might be envisaged.

## Requirements

* python3
* gcc
* cmake
* PyTango
* gevent
* numactl
* ninja

## Installation

`lima_launcher` is normally installed in a dedicated `lima_launcher` conda environment:

```bash
conda create -n lima_launcher --file requirements.txt
(conda activate lima_launcher && PREFIX=${CONDA_PREFIX} bash -e conda/build.sh)
```

The utilities `netdev_set_queue_cpu_affinity`, `set_cpu_idle_pm_state_disable` and `set_unlimited_mem_lock` must be installed system-wide (requiring `sudo` privileges):

```bash
(conda activate lima_launcher && \
   for b in netdev_set_queue_cpu_affinity \
            set_cpu_idle_pm_state_disable \
	    set_unlimited_mem_lock \
	    set_net_core_rmem_max; do \
     sudo cp ${CONDA_PREFIX}/bin/${b} /usr/local/bin; \
   done)
```

In addition, `set_unlimited_mem_lock` must provide the `CAP_SYS_RESOURCE` capability when executed:

```bash
sudo setcap cap_sys_resource+ep /usr/local/bin/set_unlimited_mem_lock
```

Finally, the `cpufrequtils` package is needed for setting the `cpufreq_governor` option:

```bash
sudo apt-get install cpufrequtils
```

## Usage

`lima_launcher` configuration is stored in JSON format. Different command line options allow to specify the JSON configuration as a command line argument, in a file or in the `Tango` database:

```
% lima_launcher --help
usage: lima_launcher [-h] [--json_config JSON_CONFIG] [--json_config_file JSON_CONFIG_FILE] [--tango_personal_name TANGO_PERSONAL_NAME] [--verbose]

Lima2 launcher.

optional arguments:
  -h, --help            show this help message and exit
  --json_config JSON_CONFIG
                        JSON configuration string
  --json_config_file JSON_CONFIG_FILE
                        JSON configuration file
  --tango_personal_name TANGO_PERSONAL_NAME
                        Tango personal name
  --skip_set_cpu_affinity
                        Do not execute set_cpu_affinity
  --verbose             Verbose output
```

When the `Tango` database is used, the information is stored in a server-like structure, where the executable name is `lima_launcher`. For each personal (instance) name, a single device of the `LimaLauncher` class should be defined, with the `config` property containing the JSON configuration.

## Supported applications

The `application` top-level configuration property defines the specific JSON schema.

### LImA2 Tango application

As a convention, the `LImA2 Tango` application stores its configuration in the `Tango` database. The JSON configuration has the following structure for a single-binary application (with control and receiver merged in a single process) just requiring a dedicated `Conda` environment:

```json
{
    "application": "lima2_tango",
    "lima2_tango": {
        "plugin": "simulator",
        "instance": "simulator_ahoms0"
    },
    "environment": {
        "*": {
            "conda": {
                "base": "/home/ahoms/conda/miniconda",
                "environment": "jungfrau_lima2"
            }
        }
    }
}
```
 
The following command starts the previous configuration, assuming that it is included in the `Tango` database inside a `lima_launcher/lima2_simulator_ahoms0` server:

```bash
lima_launcher --tango_personal_name=lima2_simulator_ahoms0 --verbose
```

A more optimized configuration with two independent receiver processes, CPU affinity tuning and hardware GZIP acceleration can have the following structure:

```json
{
    "application": "lima2_tango",
    "lima2_tango": {
        "plugin": "psi",
        "processing": "lima1",
        "instance": "jungfrau_500k_307_lid29p9jfrau1_rr_x1",
        "control": {"host_name": "lid29p9jfrau1"},
        "receivers": [
            {
                "host_name": "lid29p9jfrau1",
                "nb_receivers": 1,
                "numactl": {"node": 0}
            },
            {
                "host_name": "lid29p9jfrau1",
                "nb_receivers": 1,
                "numactl": {"node": 8}
            }
        ]
    },
    "environment": {
        "lid29p9jfrau1": {
            "conda": {
                "base": "/home/ahoms/conda/miniconda",
                "environment": "jungfrau_lima2"
            },
            "ld_preload": "/home/ahoms/p9/power-gzip/lib/libnxz.so",
            "cpu_affinity": {
                "lima": {"ranges": [[  4,  30], [ 36,  62], [ 68,  94], [100, 126]]},
                "other": {"cpus": [ 30,  62,  31,  95]},
                "net_dev": [
                    {"devs": ["enP5p1s0f0", "enP5p1s0f1",
                              "enP48p1s0f1",
                              "mlx100c0p1","mlx100c1p1"],
                     "affinity": [{"queue": -1,
                                   "affinity": {"irq":  {"cpus": [ 63]},
                                                "proc": {"cpus": [127]}}}]},
                    {"devs": ["enP48p1s0f0"],
                     "affinity": [{"queue": -1,
                                   "affinity": {"irq":  {"cpus": [ 94]},
                                                "proc": {"cpus": [126]}}}]},
                    {"devs": ["mlx100c0p0"],
                     "affinity": [{"queue": -1,
                                   "affinity": {"irq":  {"cpus": [  0,  32]},
                                                "proc": {"cpus": [  1,  33]}}}]},
                    {"devs": ["mlx100c1p0"],
                     "affinity": [{"queue": -1,
                                   "affinity": {"irq":  {"cpus": [ 64,  96]},
                                                "proc": {"cpus": [ 65,  97]}}}]}
                ]
            }
        }
    }
}
```
